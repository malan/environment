Environment with multiple programming languages (C, C++, Fortran, Rust, Java, OCaml, Julia, Haskell, IPython, CLISP), standard editors (emacs, vim, nano) and several shells (bash, fish, zsh, csh).

[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fmalan%2Fenvironment/HEAD?urlpath=/lab/workspaces/terminal)
